<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Dasar</title>
</head>
<body>
    <?php 
    echo "<h2> String PHP</h2><br>";
    $hello = "Hello! Selamat datang di Website AGNEZ MO";
    echo "Kalimat: $hello <br>";
    echo "Panjang string: " . strlen($hello);

    $passwordasli = "belajarPHP";
    $passwordinput= "belajar";

    echo "<br><br>";
    echo "Password satu: belajarPHP <br>
    Password dua: belajar <br>
    Keterangan: ";
    
    $output = strcmp($passwordasli, $passwordinput);

    if ($output !==0 ){
        echo "Password salah!";
    }else{
        echo "Password benar!";
    }

    echo "<br><br>";

    $kalimat = "Saya sedang menulis script PHP";
    $kataKunci = "menulis";
    $hasil = strpos($kalimat, $kataKunci);
    $jumlahKata=  str_word_count($kalimat);
    $ambilKata= substr($kalimat, 20, 6);

    echo "Posisi kata $kataKunci berada di karakter: ". $hasil. "<br>";
    echo "Jumlah kata dalam kalimat di atas: ". $jumlahKata . "<br>";
    echo "Mengambil kata script: " . $ambilKata . "<br> <br>";

    echo "<h2> Array PHP</h2><br>";
    $mahasiswa = array("Afan", "Dina", "Fuad");
    print_r($mahasiswa);

    echo "<br>";
    //Tambah 1 array
    $mahasiswa[]= "Lia";
    print_r($mahasiswa);

    echo "<br>";
    //menambah data lebih dari satu
    array_push($mahasiswa, "Dino", "Fahri");
    print_r($mahasiswa);

    echo "<br>";
    echo "Panjang Array: " . count($mahasiswa);
    //Memilih data pada array
    echo "<br> Mahasiswa Pertama: ". $mahasiswa[0] . "<br>";

    $mhs = [
        "no_absen" => 1,
        "nama" => "Afan",
        "nilai" => 98
    ];
    print_r($mhs);
    echo "<br>";

    //Menambah key value
    $mhs["subjek"] = "Mtk";
    print_r($mhs);

    //Array Multidemensional
    echo "<br><br>";
    $kelasb = array(
        array("B001", "Afan", 90),
        array("B002", "Nia", 89),
        array("B003", "Fuad", 87)
    );

    echo "<pre>";
    print_r($kelasb);
    echo "</pre>";

    ?>
    
</body>
</html>